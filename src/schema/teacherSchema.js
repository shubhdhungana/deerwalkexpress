/*
Teacher=[
{name:"nitan",address:"gagalphedi"},
{name:"ram",address:"gokerna"},
];

*/

import { Schema } from "mongoose";

// first make schema object since array of oject has 2 things(arrayname, object)



// making object (schema)
export let teacherSchema = Schema({
    name: {
        type: String,
        required: [true, "name is required"]

    },
    age : {
        type: String,
        required: [true, "age is required"]
    },
    isMarried: {
        type: Boolean,
        required: [true, "isMarried is required"]
    },
    subject: {
        type: String,
        required: [true, "subject is required"]
    }
})