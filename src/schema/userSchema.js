// 1st step
import { Schema } from "mongoose";
export let userSchema = Schema({
    profileImage: {
        type: String,
        required: true,
    },  
    name: {
        type: String,
        required: [true, "name field is required"]},
   
    address: {
        type: String,
        required: [true, "address field is required"],
    },
    phoneNumber: {
        type: Number,
        required: [true, "phoneNumber field is required"],
    },
    email: {
        type: String,
        required: [true, "email field is required"],
        unique: true,
    },
    password: {
        type: String,
        required: [true, "password field is required"],
    }
},{timestamps:true}) // end