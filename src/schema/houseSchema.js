// 1st step
import { Schema } from "mongoose";
export let houseSchema = {
    kitchenId: {
        type: Schema.ObjectId, // to accept id
        ref: "Kitchen", // from arrayname of kitchen model
        required: [true, "color field is required"],
    },
    gateId: {
        type: Schema.ObjectId, // to accept id
        ref: "Gate", // from arrayname of gate model
        required: [true, "basin field is required"],
    },
    description: {
        type: String,
        required: [true, "fridge field is required"],
    }
    
} // end