// model means definning array
// for model, name and object is needed

import {model} from "mongoose"
import { studentSchema } from "./studentSchema.js"
import { teacherSchema } from "./teacherSchema.js"
import { traineeSchema } from "./traineeSchema.js"
import { classroomSchema } from "./classroomSchema.js"
import { departmentSchema } from "./departmentSchema.js"
import { dataSchema } from "./dataSchema.js"
import { collegeSchema } from "./collegeSchema.js"
import { bagSchema } from "./bagSchema.js"
import { diarySchema } from "./diarySchema.js"
import { bookSchema } from "./bookSchema.js"
import { productSchema } from "./productSchema.js"
import { userSchema } from "./userSchema.js"
import { reviewSchema } from "./reviewSchema.js"
import { kitchenSchema } from "./kitchenSchema.js"
import { gateSchema } from "./gateSchema.js"
import { houseSchema } from "./houseSchema.js"

export let House = model("House", houseSchema)
export let Gate = model("Gate", gateSchema)
export let Kitchen = model("Kitchen", kitchenSchema)
export let Review = model("Review", reviewSchema)
export let User = model("User", userSchema)
export let Product = model("Product", productSchema)
export let Book = model("Book", bookSchema)
export let Diary = model("Diary", diarySchema)
    //   VariableName    arrayname   object(importing this from schema)

export let Bag = model("Bag", bagSchema)
export let College = model("College", collegeSchema)
    //   VariableName    arrayname   object(importing this from schema)


export let Data = model ("Data", dataSchema) // Data, put in singular
    //   VariableName    arrayname   object
// this variable data will be imported in controller file

export let Student = model("Student", studentSchema) // notice variable and array name is same
// variable and array name should be same
// model(array ) has two part name of array and object(schema)
// student means name of array
// the model (array) name must be singular because later mongodb can add 's' audomatically

export let  Teacher = model("Teacher", teacherSchema) // this is teacher array of object
// with teacher arr name and teacherSchema object defined

export let Trainee = model("Trainee", traineeSchema) // array of object

export let Classroom = model("Classroom", classroomSchema)
export let Department = model("Department", departmentSchema)

