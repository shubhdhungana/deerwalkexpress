// 1st step of crud
import { Schema } from "mongoose";


export let dataSchema = {
    name: {
        type: String,
        required: [true, "name is required"],
        lowercase: true,
        // uppercase: true,
        trim: true,
        minLength: [3, "name must be at least 3 characters long"],
        maxLength: [30, "name must be at least 30 characters long"],
        validate: (value)=>{
            if(/^[A-Za-z0-9]+$/.test(value)){

            }else {
                throw new Error("Name should include apha numerical values")
            }
        } // end of validate

    }, // end of name
    password: {
        type: String,
        required: [true, "password is required"],
        trim: true,

        validate: (value)=>{
            let isValidPassowrd = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()-_=+{};:'",.<>?/\\[\]|]).{8,15}$/
            if(isValidPassowrd.test(value)){

            }else {
                throw new Error("password should include apha numerical values")
            }
        } // end of validate
    },
    roll: {
        type: Number,
        required: [true, "roll is required"],
        trim: true,

    },
    isMarried: {
        type: Boolean,
        required: [true, "isMarried is required"]
    },
    spouseName: {
        type: String,
        required: [true, "spouseName is required"]
    },
    email: {
        type: String,
        required: [true, "email is required"],
        unique: true,
        validate: (value)=>{
            let isValidEmail = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test(value)
            if (isValidEmail){

            }else {
                throw new Error("email must be valid")
            }
        },
    },
    gender: {
        type: String,
        required: [true, "gender is required"],
        validate: (value)=>{
            if (value === "male" || value === "female" || value === "other"){

            }else {
                throw new Error("gender should either be male, female or other")
            }
        } // validate end
    },
    phoneNumber: {
        type: String,
        required: [true, "phoneNumber is required"],
        validate: (value)=>{
            if (String(value).length === 10){

            }else {
                throw new Error("gender should either be male, female or other")
            }
        } // validate end
    },
    dob: {
        type: Date,
        required: [true, "dob is required"]
    },
    location: { //object
        country: {
            type: String,
            required: [true, "country is required"]
        },
        exactLocation: {
            type: String,
            required: [true, "exactLocation is required"]
        },
    }, //end of location
    favTeacher: [ //array of strings
        {
            type: String,
            
        },
    ],
    favSubject: [ //arr of objects
        {
            bookName: {
                type: String,
                required: [true, "bookName is required"]
            },
            bookAuthor: {
                type: String,
                required: [true, "bookAuthor is required"]
            },
        }, // only on object since in arr
    ] // end of favsubject


} // ennd of schema