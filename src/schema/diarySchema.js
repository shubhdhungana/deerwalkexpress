// 1st step, Schema design 

import { Schema } from "mongoose";
export let diarySchema = Schema({ // to import it in model(array)
    gender: {
        type: String,
        required: [true, "gender is required"]
    },
    dob: {
        type: Date, // datatype only in mongodb
        required: [true, "gender is required"],
    },
    location: { // object
        country: {
            type: String,
            required: [true, "country is required"]
        },
        exactLocation: {
            type: String,
            required: [true, "country is required"]
        },
    },
    favTeacher: [ // arr of strings //make it only one
        { // this is syntax for string
            type: String,
        },
    ],
    favSubject: [ // array of objects
        { // only one since array of objects
            bookName: {
                type: String,
                required: [true, "bookName is required"]
            },
            bookAuthor: {
                type: String,
                required: [true, "bookAuthor is required"]
            },
        },
    ], // end of array of object

})

