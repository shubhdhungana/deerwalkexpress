/*

classRoom=> , name, numberofBench, hasTv


*/

import { Schema } from "mongoose";

// first make schema object since array of oject has 2 things(arrayname, object)


// making object (schema)
export let classroomSchema = Schema({
    name: {
        type: String,
        required: true,
    },
    numberOfBench : {
        type: Number,
        required: true,
    },
    hasTv : {
        type: Boolean,
        required: true,
    },
    
})