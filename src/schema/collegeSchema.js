// step 1 : making college schema
// schema means creating object first, so we are creating object
// it will be integrated in model(array) in another file

import { Schema } from "mongoose";

export let collegeSchema = {
    collegeName: {
        type: String,
        required: true,
    },
    classNumber: {
        type: Number,
        required: true,
    },
    canteenName: {
        type: String,
        required: true,
    },
    parkingNumber: {
        type: Number,
        required: true,
    }
}