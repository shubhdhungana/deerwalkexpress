// 1st step
import { Schema } from "mongoose";
export let bookSchema = {
    name: {
        type: String,
        required: [true, "name field is required"],
    },
    author: {
        type: String,
        required: [true, "name field is required"],
    },
    price: {
        type: Number,
        required: [true, "name field is required"],
    },
    isAvailable: {
        type: Boolean,
        required: [true, "name field is required"],
    },


} // end