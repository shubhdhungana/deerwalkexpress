/*
department=> name, hod, totalmember


*/

import { Schema } from "mongoose";


// first make schema object since array of oject has 2 things(arrayname, object)


// making object (schema)
export let departmentSchema = Schema({
    name: {
        type: String,
        required: true,
    },
    hod : {
        type: String,
        required: true,
    },
    totalMember : {
        type: Number,
        required: true,
    },
    
})
