import {Schema} from "mongoose"
/////////////////////////////////////
/*
mindmaps of array of object
-----------------------------
Below is array of object and we need to enter this in database
Student = [
    {name:"nitan", age:29, isMarried:false}.
    {name:"ram", age:39, isMarried:true}.
]
--------------------
arrayName = [{object1}, {object2}, {object3}]
Since objects are not defined. So we need to define object(schema) and them arr(model)
*/
//////////////////////////////////////////////////
/////////////////////////////////////////////
export let studentSchema = Schema({ // there is curly braces because we're defining object
    /*
    the data we're sending from front end form like name (it should be string
        and there should be name field), age(it should be number), and so on.
    */

    name:{
        type:String, // name that is sent from backend should in string so
        required: true, // need to send from backend to database, so true
    },
    age: {
        type:Number,
        required:true,
    },
    isMarried: {
        type: Boolean,
        required: true,
    }
}) // first we need to define object(schema)

