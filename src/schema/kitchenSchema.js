// 1st step
import { Schema } from "mongoose";
export let kitchenSchema = {
    color: {
        type: String,
        required: [true, "color field is required"],
    },
    basinNumber: {
        type: Number,
        required: [true, "basin field is required"],
    },
    fridgeNumber: {
        type: Number,
        required: [true, "fridge field is required"],
    }
    
} // end