// 1st step
import { Schema } from "mongoose";
export let reviewSchema = {
    productId: {
        type: Schema.ObjectId, //this means it should be in id form
        ref: "Product", // product model name this will reference to product id
        required: [true, "productID field is required"],
    },
    userId: {
        type: Schema.ObjectId, // id type or it means id should be in this form
        ref: "User", // product model name this will fetch product id
        required: [true, "userID field is required"],
    },
    description: {
        type: String,
        required: [true, "description field is required"],
    },


} // end