/*
trainee=> name, class, facaulty


*/

import { Schema } from "mongoose";

// first make schema object since array of oject has 2 things(arrayname, object)


// making object (schema)
export let traineeSchema = Schema({
    name: {
        type: String,
        required: true,
    },
    className : {
        type: String,
        required: true,
    },
    faculty : {
        type: String,
        required: true,
    }
})