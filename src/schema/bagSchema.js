// 1st step
import { Schema } from "mongoose";
export let bagSchema = {
    name: {
        type: String,
        required: [true, "name field is required"],
    },
    address: {
        type: String,
        required: [true, "address field is required"],
    },
    badgeNumber: {
        type: String,
        required: [true, "badge number field is required"]
    },
    color: { // object
        surfaceColor: {
            type: String,
            required: [true, "surface color is required"]
        },
        exactColor: {
            type: String,
            required: [true, "exact color is required"]
        }
    },
    size: [ // array of string
        {// since string so 
            type: String
        }, 
    ],
    variants: [ // array of objects
        { // only one because it(object) is inside array
            variantName: {
                type: String,
            },
            variantColor: {
                type: String,
            },
        },
    ]




} // end