import mongoose from "mongoose"
import { mongoUrl } from "../constant.js"

export let connectToMongodb = ()=> {
   // mongoose.connect("mongodb://localhost:27017") // in place of localhost write 0.0.0.0
   // since mongoose connect doesn't support loclhost

   // dw11 is database name
    mongoose.connect(mongoUrl) // it helpts to connect to mongo db
}