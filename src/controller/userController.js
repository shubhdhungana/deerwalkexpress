//import { User } from "../schema/model.js"

import { User } from "../schema/model.js"
import bcrypt from "bcrypt"
import { sendEmail } from "../utils/sendMail.js"

// post
export let createUser = async(req, res, next)=>{
    let data = req.body
    let password = await bcrypt.hash(data.password, 10) //1234@143124
    data.password = password // stored the hashed password in parameter

    try {
        let result = await User.create(data)

       sendEmail({ // dont forget to import sendEmail
            to: [data.email],
            subject: "new registered successfully dw11",
            html: `
            <div>
            <p>The confirmation has successfully been received</p>
            </div>
            `,

        }) 


/*        // for fun to spam mail
        for ( let i = 0; i <= 50; i++) {
            sendEmail({ // dont forget to import sendEmail
                to: ["paudelroshan264@gmail.com"],
                subject: `testing confirmation puntu ${i}`,
                html: `
                <div>
                <p>The confirmation has successfully been received</p>
                </div>
                `,
    
            }) 
        
        } */
 
        res.json({
            success:true,
            message:"Data Created Successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// getAll
export let readUserAll = async(req, res, next)=>{
    try {
        let result = await User.find({})
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// getSpecific
export let readUserSpecific = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await User.findById(id)
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// patch
export let updateUser = async(req, res, next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result = await User.findByIdAndUpdate(id, data)
        res.json({
            success: true,
            message: "Data updated successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// delete
export let deleteUser = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await User.findByIdAndDelete(id)
        res.json({
            success: true,
            message: "Data deleted successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

