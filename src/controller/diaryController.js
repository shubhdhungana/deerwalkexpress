import { Diary } from "../schema/model.js";


// create(post)
export let createDiary = async(req, res, next) =>{
    let data = req.body
    try {
        let result = await Diary.create(data)
        res.json({
            success: true,
            message: "DATA CREATED SUCCESSFULLY",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            result: error.message,
        })
    }
}



//readAll(get)
export let readDiaryAll  = async(req, res, next)=>{
    try {
        let result = await Diary.find({})
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}


//readSpecific(get)
export let readDiarySpecific = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Diary.findById(id)
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}



// update (patch)

export let updateDiary = async(req, res, next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result = await Diary.findByIdAndUpdate(id, data)
        res.json({
            success: true,
            message: "Data updated successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}


// delete 
export let deleteDiary = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Diary.findByIdAndDelete(id)
        res.json({
            success: true,
            message: "Data deleted successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}
