import { Product } from "../schema/model.js"

// post
export let createProduct = async(req, res, next)=>{
    let data = req.body
    try {
        let result = await Product.create(data)
        res.json({
            success:true,
            message:"Data Created Successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// getAll
export let readProductAll = async(req, res, next)=>{
    try {
        let result = await Product.find({})
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// getSpecific
export let readProductSpecific = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Product.findById(id)
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// patch
export let updateProduct = async(req, res, next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result = await Product.findByIdAndUpdate(id, data)
        res.json({
            success: true,
            message: "Data updated successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// delete
export let deleteProduct = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Product.findByIdAndDelete(id)
        res.json({
            success: true,
            message: "Data deleted successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

