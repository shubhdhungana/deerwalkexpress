//import { Kitchen } from "../schema/model.js"

import { Kitchen } from "../schema/model.js"

// post
export let createKitchen = async(req, res, next)=>{
    let data = req.body
    try {
        let result = await Kitchen.create(data)
        res.json({
            success:true,
            message:"Data Created Successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// getAll
export let readKitchenAll = async(req, res, next)=>{
    try {
        let result = await Kitchen.find({})
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// getSpecific
export let readKitchenSpecific = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Kitchen.findById(id)
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// patch
export let updateKitchen = async(req, res, next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result = await Kitchen.findByIdAndUpdate(id, data)
        res.json({
            success: true,
            message: "Data updated successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// delete
export let deleteKitchen = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Kitchen.findByIdAndDelete(id)
        res.json({
            success: true,
            message: "Data deleted successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

