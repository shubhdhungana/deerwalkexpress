import { Classroom } from "../schema/model.js";

// post
export let createClassroom = async(req, res, next)=>{
    let data = req.body
    try {
        let result = await Classroom.create(data)
        res.json({
            success: true,
            message: "Data created successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message
        })
    }
}

//getAll
export let readClassroomAll = async(req, res, next)=>{
    try {
        let result = await Classroom.find({})
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message
        })
    }
}

//getSpecific
export let readClassroomSpecific = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Classroom.findById(id)
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message
        })
    }
}

//patch
export let updateClassroom = async(req, res, next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result = await Classroom.findByIdAndUpdate(id, data)
        res.json({
            success: true,
            message: "Data Updated Successfully",
            result: result
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
    
}

// delete
export let deleteClassroom = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Classroom.findByIdAndDelete(id)
        res.json({
            success: true,
            message: "Classroom Deleted Successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
    
}