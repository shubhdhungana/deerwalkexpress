export let uploadSingleFile = (req, res, next) => {
    console.log(req.file)
    res.json({
        success: true,
        message: "file uploaded successfully",
        link: `http://localhost:8000/$(req.file.filename)`,
    })
}

export let uploadMultipleFiles = (req, res, next) =>{
    let link = req.files.map((value, i)=>{ 
        // req.files is used because upload.array is used in line 17,
        // otherwise file should be used since line 14
        return `http://localhost:8000/${value.filename}`
    })
    res.json({
        success: true,
        message: "file upload successfully",
        link: link,
    })
}