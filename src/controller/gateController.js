//import { Gate } from "../schema/model.js"

import { Gate } from "../schema/model.js"

// post
export let createGate = async(req, res, next)=>{
    let data = req.body
    try {
        let result = await Gate.create(data)
        res.json({
            success:true,
            message:"Data Created Successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}


// getAll
export let readGateAll = async(req, res, next)=>{
    try {
        let result = await Gate.find({})
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// getSpecific
export let readGateSpecific = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Gate.findById(id)
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// patch
export let updateGate = async(req, res, next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result = await Gate.findByIdAndUpdate(id, data)
        res.json({
            success: true,
            message: "Data updated successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// delete
export let deleteGate = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Gate.findByIdAndDelete(id)
        res.json({
            success: true,
            message: "Data deleted successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

