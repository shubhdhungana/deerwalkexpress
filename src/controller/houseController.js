//import { House } from "../schema/model.js"

import { House } from "../schema/model.js"

// post
export let createHouse = async(req, res, next)=>{
    let data = req.body
    try {
        let result = await House.create(data)
        res.json({
            success:true,
            message:"Data Created Successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// getAll
export let readHouseAll = async(req, res, next)=>{
    try {
        let result = await House.find({}).populate("kitchenId").populate("gateId")
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// getSpecific
export let readHouseSpecific = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await House.findById(id)
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// patch
export let updateHouse = async(req, res, next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result = await House.findByIdAndUpdate(id, data)
        res.json({
            success: true,
            message: "Data updated successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// delete
export let deleteHouse = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await House.findByIdAndDelete(id)
        res.json({
            success: true,
            message: "Data deleted successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

