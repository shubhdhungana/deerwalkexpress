import { Department } from "../schema/model.js";

// post
export let createDepartment = async(req, res, next)=>{
    let data = req.body
    try {
        let result = await Department.create(data)
        res.json({
            success:true,
            message:"Data Created Successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// getAll
export let readDepartmentAll = async(req, res, next)=>{
    try {
        let result = await Department.find({})
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// getSpecific
export let readDepartmentSpecific = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Department.findById(id)
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// patch
export let updateDepartment = async(req, res, next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result = await Department.findByIdAndUpdate(id, data)
        res.json({
            success: true,
            message: "Data updated successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// delete
export let deleteDepartment = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Department.findByIdAndDelete(id)
        res.json({
            success: true,
            message: "Data deleted successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

