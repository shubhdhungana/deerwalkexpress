//import { Review } from "../schema/model.js"

import { Review } from "../schema/model.js"

// post
export let createReview = async(req, res, next)=>{
    let data = req.body
    try {
        let result = await Review.create(data)
        res.json({
            success:true,
            message:"Data Created Successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// getAll
export let readReviewAll = async(req, res, next)=>{

    try {
    
        let result = await Review.find({}).populate("productId").populate("userId")
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// getSpecific
export let readReviewSpecific = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Review.findById(id)
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// patch
export let updateReview = async(req, res, next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result = await Review.findByIdAndUpdate(id, data)
        res.json({
            success: true,
            message: "Data updated successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// delete
export let deleteReview = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Review.findByIdAndDelete(id)
        res.json({
            success: true,
            message: "Data deleted successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

