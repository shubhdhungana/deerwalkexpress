// step 3 making controller

//importing data model(array) from model.js
import { Trainee } from "../schema/model.js";

//post
export let createTrainee = async(req, res, next)=>{
    let data = req.body
    try {
        let result = await Trainee.create(data) // insert into Trainee model
        res.json({
            success: true,
            message: "Data created Success",
            result: result
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

//getAll
export let readTraineeAll = async(req, res, next)=>{
    try {
        let result = await Trainee.find({}) // find from Trainee model
        res.json({
            success:true,
            message: "DATA found Successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// getSpecific
export let readTraineeSpecific = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Trainee.findById(id)
        res.json({
            success:true,
            message: "DATA found Successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// patch
export let updateTrainee = async(req, res, next)=>{
    let id = req.params.id
    let traineeInfo = req.body
    try {
        let result = await Trainee.findByIdAndUpdate(id, traineeInfo)
        res.json({
            success: true,
            message: "Data Updated Successfully",
            result: result
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// delete
export let deleteTrainee = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Trainee.findByIdAndDelete(id)
        res.json({
            success: true,
            message: "Data Deleted Successfully",
            result: result
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

