// step 3 making controller

//importing data model(array) from model.js
import { Book } from "../schema/model.js";

//post
export let createBook = async(req, res, next)=>{
    let data = req.body
    try {
        let result = await Book.create(data) // insert into Book model
        res.json({
            success: true,
            message: "Data created Success",
            result: result
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

//getAll
export let readBookAll = async(req, res, next)=>{
    try {
        let result = await Book.find({}) // find from Book model
        res.json({
            success:true,
            message: "DATA found Successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// getSpecific
export let readBookSpecific = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Book.findById(id)
        res.json({
            success:true,
            message: "DATA found Successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// patch
export let updateBook = async(req, res, next)=>{
    let id = req.params.id
    let bookInfo = req.body
    try {
        let result = await Book.findByIdAndUpdate(id, bookInfo)
        res.json({
            success: true,
            message: "Data Updated Successfully",
            result: result
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// delete
export let deleteBook = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Book.findByIdAndDelete(id)
        res.json({
            success: true,
            message: "Data Deleted Successfully",
            result: result
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

