//import { Teacher } from "../schema/model.js";
import { Teacher } from "../schema/model.js"

// post
export let createTeacher = async(req, res, next)=>{
    let data = req.body
    try {
        let result = await Teacher.create(data)
        res.json({
            success:true,
            message:"Data Created Successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// getAll
export let readTeacherAll = async(req, res, next)=>{
    let limit = req.query.limit
    let page = req.query.page
    try {
        let result = await Teacher.find({}).skip((page - 1)*limit).limit(limit)
        // above is pagination formula, above is formula
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// getSpecific
export let readTeacherSpecific = async(req, res, next)=>{

    let id = req.params.id
    try {
        let result = await Teacher.findById(id)
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// patch
export let updateTeacher = async(req, res, next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result = await Teacher.findByIdAndUpdate(id, data)
        res.json({
            success: true,
            message: "Data updated successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// delete
export let deleteTeacher = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Teacher.findByIdAndDelete(id)
        res.json({
            success: true,
            message: "Data deleted successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

