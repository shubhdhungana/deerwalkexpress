import { Data } from "../schema/model.js";

// post
export let createData = async(req, res, next)=>{
    let data = req.body
    try {
        let result = await Data.create(data)
        res.json({
            success:true,
            message:"Data Create Successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}


// get all
export let readDataAll = async(req, res, next)=>{
    
let limit = req.query.limit
let page = req.query.page
    try {
        // while searching we focus on value not type
        // suppose if there is string 50 instea of number 50 like below
        // the result will be same because it focus on value not type
        //let result = await Data.find({name:"nitan", roll: 50}) // this only gets nitan data and roll 50 data

       // let result = await Data.find({roll: { $gt: 50 }}) // to get roll number value greater than 50
        // let result = await Data.find({roll: { $gte: 50 }}) // to get roll number value greater than  or equal to 50

      //  let result = await Data.find({age: {$lt: 70}}) // less than 70 searches
      //let result = await Data.find({age:{$lte:50}}) ==> searches less than equal to 70
      // let result = await Data.find({age:{$ne:50}}) ==> not equal to 50
    // let result = await Data.find({roll:{$in: [20, 25, 30]}}) // it includes roll number 20, 25, 30
       // let result = await Data.find({roll:{$gte:20, $lte: 25}}) // it includes roll number ranging from 20 to 25
    //string searching (searches including nitan and ram)
   // let result = await Data.find({name: {$in: ["nitan", "ram"]}}) 
    
   // Get validation regex searching
// let result = await Data.find({name:"nitan"})
// let result = await Data.find({name:/nitan/})
// let result = await Data.find({name:/nitan/i})
// let result = await Data.find({name:/ni/})
// let result = await Data.find({name:/ni/i})
// let result = await Data.find({name:/^ni/})
// let result = await Data.find({name:/^ni/i})
// let result = await Data.find({name:/ni$/})

// this(select) only gives result of name and gender also it hides id by -
// find has control over the object where as select as control ove the object property
//let result = await Data.find({}).select("name gender -_id")
// id is an exception so use - _id , it'll display in all otherwise
// below result shows except name and gender
//let result = await Data.find({}).select("-name -gender")
//let result = await Data.find({}).select("name -gender") // not valid [no use of + and -]
//let result = await Data.find({}).select("-name gender") // not valid [no use of + and -]

// get string searching
//.find({}).sort("name") // ascending sorting
//.find({}).sort("-name") // decreasing order sorting
//.find({}).sort("name age") // both sorting
//.find({}).sort("name -age") //ascending name and then descending age
//.find({}).sort("-name age")


//.find({}).skip("4") // skips first 4 data and show rest datas
//.find({}).limit("4") // only shows first 4 data
//.find({}).limit("5").skip("2")


let result = await Data.find({})
        res.json({
            success:true,
            message:"Data found Successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}


// get specific
export let readDataSpecific = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Data.findById(id)
        res.json({
            success:true,
            message:"Data found Successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}


// patch
export let updateData = async(req, res, next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result = await Data.findByIdAndUpdate(id, data, {new: true})
        // if {new: false} it give old data
        // if {new: true} it give new data
        res.json({
            success: true,
            message: "Data Updated Successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
            
        })
    }
}

// delete
export let deleteData = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Data.findByIdAndDelete(id)
        res.json({
            success: true,
            message: "Data deleted successfully",
            result: result
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}


