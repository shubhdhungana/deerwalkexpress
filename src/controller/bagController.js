import { Bag } from "../schema/model.js";

// post
export let createBag = async(req, res, next)=>{
    let data = req.body
    try {
        let result = await Bag.create(data)
        res.json({
            success:true,
            message:"Data Created Successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// getAll
export let readBagAll = async(req, res, next)=>{
    try {
        let result = await Bag.find({})
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// getSpecific
export let readBagSpecific = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Bag.findById(id)
        res.json({
            success: true,
            message: "Data found successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// patch
export let updateBag = async(req, res, next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result = await Bag.findByIdAndUpdate(id, data)
        res.json({
            success: true,
            message: "Data updated successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// delete
export let deleteBag = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await Bag.findByIdAndDelete(id)
        res.json({
            success: true,
            message: "Data deleted successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

