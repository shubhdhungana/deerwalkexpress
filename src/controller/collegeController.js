// step 3 making controller

//importing data model(array) from model.js
import { College } from "../schema/model.js";

//post
export let createCollege = async(req, res, next)=>{
    let data = req.body
    try {
        let result = await College.create(data) // insert into College model
        res.json({
            success: true,
            message: "Data created Success",
            result: result
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

//getAll
export let readCollegeAll = async(req, res, next)=>{
    try {
        let result = await College.find({}) // find from College model
        res.json({
            success:true,
            message: "DATA found Successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// getSpecific
export let readCollegeSpecific = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await College.findById(id)
        res.json({
            success:true,
            message: "DATA found Successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// patch
export let updateCollege = async(req, res, next)=>{
    let id = req.params.id
    let collegeInfo = req.body
    try {
        let result = await College.findByIdAndUpdate(id, collegeInfo)
        res.json({
            success: true,
            message: "Data Updated Successfully",
            result: result
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

// delete
export let deleteCollege = async(req, res, next)=>{
    let id = req.params.id
    try {
        let result = await College.findByIdAndDelete(id)
        res.json({
            success: true,
            message: "Data Deleted Successfully",
            result: result
        })
    } catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }
}

