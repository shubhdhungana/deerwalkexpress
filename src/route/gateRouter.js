
import { Router } from "express"
import { createGate, deleteGate, readGateAll, readGateSpecific, updateGate } from "../controller/gateController.js"

// this will be imported in index file
export let gateRouter = Router()

// first route
gateRouter.route("/")
.get(readGateAll)
//.post(createGate)

.post((req, res, next)=>{
    console.log("first middleware")
    req.name = "nitan"
    next()
}, (req, res, next)=>{
    console.log(req.name)
    console.log("2nd middleware")
})

// id route
gateRouter.route("/:id")
.get(readGateSpecific)
.patch(updateGate)
.delete(deleteGate) 