// 4th step

import { Router } from "express";
import { createDiary, deleteDiary, readDiaryAll, readDiarySpecific, updateDiary } from "../controller/diaryController.js";


export let diaryRouter = Router()

diaryRouter.route("/")
.get(readDiaryAll)

diaryRouter.route("/:id")
.post(createDiary)
.get(readDiarySpecific)
.patch(updateDiary)
.delete(deleteDiary)