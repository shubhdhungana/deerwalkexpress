/*
import { Router } from "express"; // importing default
import { createUser, deleteUser, readUserAll, readUserSpecific, updateUser } from "../controller/userController.js";

*/
import { Router } from "express"
import { uploadMultipleFiles, uploadSingleFile } from "../controller/fileController.js"
import upload from "../utils/fileUpload.js"

// this will be imported in index file
export let fileRouter = Router()

// first route
fileRouter.route("/single").post(upload.single("profileImage"),uploadSingleFile)

// next api
fileRouter.route("/multiple").post(upload.array("profileImage"),uploadMultipleFiles)

