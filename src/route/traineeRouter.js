import { Router } from "express"; // importing default
import { createTrainee, deleteTrainee, readTraineeAll, readTraineeSpecific, updateTrainee } from "../controller/traineeController.js";

// this will be imported in index file
export let traineeRouter = Router()

// first route
traineeRouter.route("/")
.get(readTraineeAll)
.post(createTrainee)

// id route
traineeRouter.route("/:id")
.get(readTraineeSpecific)
.patch(updateTrainee)
.delete(deleteTrainee)