/*import { Router } from "express"; // importing default
import { createReview, deleteReview, readReviewAll, readReviewSpecific, updateReview } from "../controller/reviewController.js";
*/
import { Router } from "express"
import { createReview, deleteReview, readReviewAll, readReviewSpecific, updateReview } from "../controller/reviewController.js"

// this will be imported in index file
export let reviewRouter = Router()

// first route
reviewRouter.route("/")
.get(readReviewAll)
.post(createReview)

// id route
reviewRouter.route("/:id")
.get(readReviewSpecific)
.patch(updateReview)
.delete(deleteReview)