import { Router } from "express";
import { createClassroom, deleteClassroom, readClassroomAll, readClassroomSpecific, updateClassroom } from "../controller/classroomController.js";


export let classroomRouter = Router()

//first
classroomRouter.route("/")
.get(readClassroomAll)

// second
classroomRouter.route("/:id")
.post(createClassroom)
.get(readClassroomSpecific)
.patch(updateClassroom)
.delete(deleteClassroom)