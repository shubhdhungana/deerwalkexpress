/*import { Router } from "express"; // importing default
import { createProduct, deleteProduct, readProductAll, readProductSpecific, updateProduct } from "../controller/productController.js";
*/
import { Router } from "express"
import { createProduct, deleteProduct, readProductAll, readProductSpecific, updateProduct } from "../controller/productController.js"

// this will be imported in index file
export let productRouter = Router()

// first route
productRouter.route("/")
.get(readProductAll)
.post(createProduct)

// id route
productRouter.route("/:id")
.get(readProductSpecific)
.patch(updateProduct)
.delete(deleteProduct)