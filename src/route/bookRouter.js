import { Router } from "express"; // importing default
import { createBook, deleteBook, readBookAll, readBookSpecific, updateBook } from "../controller/bookController.js";

// this will be imported in index file
export let bookRouter = Router()

// first route
bookRouter.route("/")
.get(readBookAll)
.post(createBook)

// id route
bookRouter.route("/:id")

.get(readBookSpecific)
.patch(updateBook)
.delete(deleteBook)