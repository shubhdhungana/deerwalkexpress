
import { Router } from "express"
import { createHouse, deleteHouse, readHouseAll, readHouseSpecific, updateHouse } from "../controller/houseController.js"

// this will be imported in index file
export let houseRouter = Router()

// first route
houseRouter.route("/")
.get(readHouseAll)
.post(createHouse)

// id route
houseRouter.route("/:id")
.get(readHouseSpecific)
.patch(updateHouse)
.delete(deleteHouse)