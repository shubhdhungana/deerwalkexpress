import { Router } from "express";
export let secondRouter = Router()

/*
syntax of first middleware function, 2nd middleware function

syntax
.get(()=>{} ,(){})

.get(firstMiddleWare, secondMiddleWare)
*/

secondRouter.route("/") // localhost:8000/second
.get((req, res, next)=>{
    console.log("this is first middleware function") //prints on backend console
    next()
}, (req, res, next)=>{
    console.log("this is second middleware function")  //prints on backend console
})


// fetching datas from front end to backend
secondRouter.route("/a/:id/b/:name") // this is just query
.get((req, res, next)=>{
    console.log(req.body) // fetching user value datas in body
    console.log(req.params) // fetches params (before ?) from frontend to backend
    console.log(req.query) // fetches query(after ?) from frotend
    res.json({success:true}) // message sent to front end
})

