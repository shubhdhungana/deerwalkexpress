
import { Router } from "express"
import { createKitchen, deleteKitchen, readKitchenAll, readKitchenSpecific, updateKitchen } from "../controller/kitchenController.js"

// this will be imported in index file
export let kitchenRouter = Router()

// first route
kitchenRouter.route("/")
.get(readKitchenAll)
.post(createKitchen)

// id route
kitchenRouter.route("/:id")
.get(readKitchenSpecific)
.patch(updateKitchen)
.delete(deleteKitchen)