// if someone type certain urls, do make certain response

import { Router } from "express";
export let firstRouter = Router()

firstRouter
// since localhost:800/bike already configured in index.js
.route("/") //localhost:8000/bike

/*
syntax (two syntaxes in middleware)
.get(()=>{} ,()=>{})
*/
.get((req, res, next)=>{
    console.log("i am middleware 1")
    next() // next is used to trigger next middleware function
},(req, res,next)=>{ // 2nd middleware function
    console.log("I am middleware 2")
}) 

.post((req, res, next)=>{
    let data = req.body 
    // req.body enables to receive datas from postman to backend
    console.log(data) // this prints postman info to backend 
    res.json(data) // according to front end data received, message to frontend

}) 
.patch((req, res, next)=>{
    let data = req.body
    console.log(data) // prints postman info to console/backend
    res.json(data) // sends backend data obtained to postman
}) 
.delete((req, res, next)=>{
    res.json("this is bike delete")
}) 

