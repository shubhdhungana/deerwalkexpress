// now doing for separate urls

// importing router from express
import { Router } from "express"
export let schoolsRouter = Router()

schoolsRouter.route("/") // route / means localhost:8000/schools (see index.js file)
.get((req, res, next)=>{
    console.log(req.body) // this req.body takes front end data 

    res.json( {success:true, message:"school read successfully"})
    // res.json work is to print backend stuffs to front end(postman)
})
.post((req, res, next)=>{
    console.log(req.body) // this req.body takes front end data 
    res.json( {success:true, message:"school created successfully"})

})
.patch((req, res, next)=>{
    res.json( {success:true, message:"school updated successfully"})

})
.delete((req, res, next)=>{
    res.json( {success:true, message:"school deleted successfully"})

})
