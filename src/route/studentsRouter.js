import { Router } from "express";

import { Student } from "../schema/model.js";

export let studentRouter = Router()

// first route
studentRouter.route("/") // localhost:8000/students
.get(async(req, res, next)=>{
    try { // put it in try the required response and if error occured, catch is there
        let result = await Student.find({}) // this will execute at last default
        // so to execute at first, we should use await before student.find
        // also when you put await, you should put async before req, res like above.
        console.log("it works")
        res.json({
            success:true,
             message:"Student Read successfully",
             result: result,
    })
    } catch (error) { // if database is down or error occured while fetching
        // show this below response
        res.json({
            success: false,
            message: "Unable to read student"
        })
    }
  
})
.post(async(req, res, next)=>{
    let data = req.body
    

    try {
            /*
    Student.create is also promise or async that means it would be by default 
    executed at last. So need to use await async to execute at first.
    to check whether it is async or not, use (clg(Student.create()))
    */
    let result = await Student.create(data) // create uploads data in database
    res.json({success:true, message:"Student Create successfully"})

    }catch (error) {
        res.json ({
            success: false, 
            message: error.message,
        })
    }
})

// next route
studentRouter.route("/:id") // localhost:8000/students/1
.get(async(req, res, next)=>{

    let id = req.params.id
    try { // put it in try the required response and if error occured, catch is there
        let result = await Student.findById(id) // this will execute at last default
        // so to execute at first, we should use await before student.find
        // also when you put await, you should put async before req, res like above.
 
        res.json({
            success:true,
             message:"Student Read successfully",
             result: result,
    })
    } catch (error) { // if database is down or error occured while fetching
        // show this below response
        res.json({
            success: false,
            message:error.message
        })
    }
})
.patch(async(req, res, next)=>{
    let id = req.params.id // it fetches id from front end url 
    let data = req.body // it fetches front end data

    try {
        let result = await Student.findByIdAndUpdate(id,data,{new:true}) // syntax
        // to get student data from database by id and update it
        res.json({
            success:true,
            message:"Student Updated successfully",
            result: result,
    })

    }catch (error) {
        res.json({
            success: false,
            message: error.message,
        })
    }

})
/* .delete(async(req, res, next)=>{
    let id = req.params.id
    let result = await Student.findByIdAndDelete(id)
    try {
        if (result === null) {
            res.json({
                success:false,
                message: "Student does not exist",
            })
        }else {
            res.json({
                success: true,
                message: "student deleted succesfully",
                result: result,
            })
}
 */