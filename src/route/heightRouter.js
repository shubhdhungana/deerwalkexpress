//import
import { Router } from "express";


export let heightRouter = Router()

heightRouter.route("/")
.get((req, res, next)=>{
    console.log(req.body) // receives data from front end and prints it to backend console
    res.json("response from backend get") // give response to front end
})
.post((req, res, next)=>{
    res.json("response from backend post")
})
.patch((req, res, next)=>{
    res.json("response from backend update")
})
.delete((req, res, next)=>{
    res.json("response from backend delete")
})
