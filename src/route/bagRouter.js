import { Router } from "express"; // importing default
import { createBag, deleteBag, readBagAll, readBagSpecific, updateBag } from "../controller/bagController.js";

// this will be imported in index file
export let bagRouter = Router()

// first route
bagRouter.route("/")
.get(readBagAll)

// id route
bagRouter.route("/:id")
.post(createBag)
.get(readBagSpecific)
.patch(updateBag)
.delete(deleteBag)