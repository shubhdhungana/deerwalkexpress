import { Router } from "express";
export let errorMiddleware = Router()

errorMiddleware.route("/")
.get((req, res, next)=>{
    console.log("i am middleware 1")
    next(new Error("my error")) // error middleware syntax to trigger it
},
(err, req, res, next)=>{
    console.log(new Error("my error").message)
    console.log("i am error middleware")
    next() // trigger next normal middleware
},
(req, res, next)=>{
    
    console.log("i am middleware 2")
})

