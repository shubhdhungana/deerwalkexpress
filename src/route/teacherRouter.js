

import { Router } from "express"; // importing router first
import { createTeacher, deleteTeacher, readTeacherAll, readTeacherSpecific, updateTeacher } from "../controller/teacherController.js";


export let teacherRouter = Router()
teacherRouter.route("/") //localhost:8000/teachers
.get(readTeacherAll)
.post(createTeacher)

teacherRouter.route("/:id") //localhost:8000/teachers
.get(readTeacherSpecific)
.patch(updateTeacher)
.delete(deleteTeacher)
