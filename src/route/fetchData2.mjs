// importing here
import { Router } from "express";
export let fetchData = Router ()

// this program depicts the fetching data system
/*
we are going to fetch user data from front end () to backend (express)
i) fetching user forms data from to backend
ii) fetching params
iii) fetching query
*/
fetchData.route("/:name/:address") 
//localhost:8000/fetchData
.get((req, res, next)=>{
    console.log(req.body) // fetches front body data
    console.log(req.params) // fetches params (this is simply endpoints)
    console.log(req.query) // fetches query 
})
.post((req, res, next)=>{
    console.log("this is first middleware")
    res.json("hi there, response from middleware 1") // sending to frontend
    next()
}, (req, res, next)=>{
    console.log("this is second middleware")
    res.json("hi there, response from middleware 1") // sending to frontend
})