// importing router
import { Router } from "express";
export let fetchingData = Router()

/*
fetching user data
i) via req.body, front end body
ii) via req.params, user params (url before ? or initial url)
iii) via req.query, user query
*/

fetchingData.route("/a/:name/b/:age") // localhost:8000/fetchingData
.get((req, res, next)=>{
    console.log(req.params) // { name: 'nitan', age: '29' }
    console.log(req.query) //{ address: 'ktm' }
    console.log(req.body) // { name: 'subh', age: 23 }
})

// middleware function
/*
i) below is route middleware since it is defined in route file

ii) the middleware defined in main.js file is called application middleware

*/
.post((req, res, next)=>{
    console.log("this is 1st middlware") // this is first middleware
    next() // next triggers another middleware
},()=>{
    console.log("this is 2nd middleware") // this is second middleware
})