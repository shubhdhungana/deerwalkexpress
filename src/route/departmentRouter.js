

import { Router } from "express"; // importing router first
import { createDepartment, deleteDepartment, readDepartmentAll, readDepartmentSpecific, updateDepartment } from "../controller/departmentController.js";


export let departmentRouter = Router()
departmentRouter.route("/") 
.get(readDepartmentAll)

departmentRouter.route("/:id") 
.get(readDepartmentSpecific) 
.post(createDepartment)
.patch(updateDepartment)
.delete(deleteDepartment)
