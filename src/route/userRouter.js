/*import { Router } from "express"; // importing default
import { createUser, deleteUser, readUserAll, readUserSpecific, updateUser } from "../controller/userController.js";
*/
import { Router } from "express"
import { createUser, deleteUser, readUserAll, readUserSpecific, updateUser } from "../controller/userController.js"

// this will be imported in index file
export let userRouter = Router()

// first route
userRouter.route("/")
.get(readUserAll)
.post(createUser)

// id route
userRouter.route("/:id")
.get(readUserSpecific)
.patch(updateUser)
.delete(deleteUser)