import { Router } from "express";
import { createCollege, deleteCollege, readCollegeAll, readCollegeSpecific, updateCollege } from "../controller/collegeController.js";


export let collegeRouter = Router()

collegeRouter.route("/")
.get(readCollegeAll)

collegeRouter.route("/:id")
.post(createCollege)
.get(readCollegeSpecific)
.patch(updateCollege)
.delete(deleteCollege)