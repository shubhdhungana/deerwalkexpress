

import { Router } from "express"; // importing router first
import { createData, deleteData, readDataAll, readDataSpecific, updateData } from "../controller/dataController.js";


export let dataRouter = Router()
dataRouter.route("/") //localhost:8000/datas
.get(readDataAll)
.post(createData)

dataRouter.route("/:id") //localhost:8000/datas
.get(readDataSpecific) 
.patch(updateData)
.delete(deleteData)
