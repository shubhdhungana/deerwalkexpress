

import express, { json } from "express"
import { firstRouter } from "./src/route/firstRouter.js"
import { schoolsRouter } from "./src/route/schoolsRouter.js"
import { vehiclesRouter } from "./src/route/vehiclesRouter.js"
import { heightRouter } from "./src/route/heightRouter.js"
import { userRouter } from "./src/route/userRouter.js"
import { secondRouter } from "./src/route/secondRouter.js"
import { fetchingData } from "./src/route/fetchingData3.js"
import { fetchData } from "./src/route/fetchData2.mjs"
import { errorMiddleware } from "./src/route/errorMiddleware.js"
import { connectToMongodb } from "./src/connectToDb/connectToMongoDb.js"
import { studentRouter } from "./src/route/studentsRouter.js"
import { dataRouter } from "./src/route/dataRouter.js"
import { collegeRouter } from "./src/route/collegeRouter.js"
import { bagRouter } from "./src/route/bagRouter.js"
import { classroomRouter } from "./src/route/classroomRouter.js"
import { departmentRouter } from "./src/route/departmentRouter.js"
import { diaryRouter } from "./src/route/diaryRouter.js"
import { teacherRouter } from "./src/route/teacherRouter.js"
import { bookRouter } from "./src/route/bookRouter.js"
import { traineeRouter } from "./src/route/traineeRouter.js"
import { productRouter } from "./src/route/productRouter.js"
import bcrypt from 'bcrypt';
import { reviewRouter } from "./src/route/reviewRouter.js"
import { kitchenRouter } from "./src/route/kitchenRouter.js"
import { gateRouter } from "./src/route/gateRouter.js"
import { houseRouter } from "./src/route/houseRouter.js"
// import { config } from "dotenv";
import { config } from "dotenv"
config();

// this is commit section
let expressApp = express()
expressApp.use(json()) // this helps backend to receive json data
// this  backend favorable
expressApp.use(express.static("./public"))// this helps to get the image from public
// below is just example of modular function, this is called global middleware or application middleware
/* expressApp.use((req, res, next)=>{
    console.log("I am application middleware zero")
    next()
})  */
// upto here

connectToMongodb() // this is from file connectToMongodb.js.
// It connects express & mongodb

expressApp.use("/files", fileRouter)
expressApp.use("/houses", houseRouter)
expressApp.use("/gates", gateRouter)
expressApp.use("/kitchens", kitchenRouter)
expressApp.use("/reviews", reviewRouter)
expressApp.use("/products", productRouter)
expressApp.use("/trainees", traineeRouter)
expressApp.use("/books", bookRouter)
expressApp.use("/teachers", teacherRouter)
expressApp.use("/diaries", diaryRouter)
expressApp.use("/departments", departmentRouter) 
expressApp.use("/classrooms", classroomRouter) 
expressApp.use("/bags", bagRouter)
//           link(plural)  variable name in router file

expressApp.use("/colleges", collegeRouter)
//           link(plural)  variable name in router file

expressApp.use("/datas", dataRouter) //import from router file
//           link(plural)  variable name in router file
expressApp.use("/bike", firstRouter) // import from router file
expressApp.use("/schools", schoolsRouter) // linking it to router file
expressApp.use("/vehicles", vehiclesRouter)
expressApp.use("/height", heightRouter)
expressApp.use("/users", userRouter)
expressApp.use("/second", secondRouter)
expressApp.use("/fetchingData", fetchingData)
expressApp.use("/fetchData", fetchData)
expressApp.use("/errorMiddleware", errorMiddleware)
expressApp.use("/students", studentRouter)


expressApp.listen(port, ()=>{
    console.log("this app is listening at port 8000") 
    
    //=>this app is listening at port 8000
})


/*
making api means front end request to back end
- so how can we make front end request? By url and method:


url:"localhost:8000" [this is backend url, front end request to this url]
method: get, post, patch(update), delete

*/

/*  hasing
code to hash password
console.log(await bcrypt.hash("password@1324", 10))

 */

/* // while user log ins, user password should be equal to hashpassword
let password = "sanjayaIDNew"
let hashPassword = "$2b$10$ZYu2oyG.KM.hhN3hEnISyORyrsZ0fU83BVWfYE/EVUXxjPsPg6Ucq"
let isValidPassword = await bcrypt.compare(password, hashPassword) // comparing
console.log(isValidPassword) // true,
 */

// hashing end
 
 //generate token
import jwt from "jsonwebtoken"
import { port, secretKey } from "./src/constant.js"
import { fileRouter } from "./src/route/fileRouter.js"
let infoObj = { // any variable name and objects inside it
    _id: "12342134"
}

let expiryInfo = {
    expiresIn: "365d",
}
console.log(jwt.sign(infoObj, secretKey, expiryInfo))
 //token = eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiIxMjM0MjEzNCIsImlhdCI6MTcwNjM0MTk0MywiZXhwIjoxNzM3ODc3OTQzfQ.mGuwEmSNvBswqwnblmnRcuvew95WFZJFxjFHgdnR_EY

 
/* 
 // validate token
 let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoibml0YW4iLCJhZ2UiOjI5LCJpYXQiOjE3MDYzNDE2MDAsImV4cCI6MTczNzg3NzYwMH0._8gbcTyGUb224nq_bozDqKhjoLU-HU3eFuOPaNQDFmE"
  try {
    let inObj = jwt.verify(token, "dw11")
    console.log(inObj)
 }catch(error) {
    console.log(error.message)
 }    
  */

 /*
logic of file upload to server
[-----]
1. send file from front end
2. save file to server
 */
